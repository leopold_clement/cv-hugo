---
title: "RobotCup"
date: 2022-11-01T11:59:54+01:00
featured: true
description: "School project on small football playing robot"
tags: ["BLE","MBed","Embedded","C++", "Python"]
image: ""
link: "https://www.robocup.fr/"
fact: "School project on small football playing robot bis"
weight: 500
sitemap:
  priority : 0.8
---
### What is it ?
RobotCup is a competition between students from multiple schools and university.
The aim is to create a team of a few robot to plays football at small scale.
We build tree wheeled robot.

### What I did
I worked on the communication between the supervisor and the robots through a Bluetooth low energy connection.
I used python on the supervisor side, and C++ on the robot side.
The robot were using MBed, a framework by ARM.