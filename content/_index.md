---
title: "Home"
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
Élève Normalien à l'ENS Paris-Saclay. Agrégé de science industrielle de l'ingénieur option génie informatique.